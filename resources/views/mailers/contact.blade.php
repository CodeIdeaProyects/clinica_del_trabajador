<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Contacto desde tu página web</title>
	<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic" rel="stylesheet">
  </head>
  <body style="background-color: #EAEAEA; font-family: 'Nanum Gothic', sans-serif;">
      <div style="width: 50%; background-color: #FFFFFF; margin: 80px auto; padding-bottom: 50px;">
        <div style="width: 100%; text-align: center; margin: auto; margin-top: 40px; margin-bottom: 40px; background-color: #FFFFFF; background-image: url({{ asset('img/trama_moodle.png') }}); background-repeat: repeat; background-size: 150px 150px; padding: 20px 0px;">
          <img src="{{ asset('img/Logo_curvas.png') }}" style="width: 150px; margin: auto;">
        </div>
        <h3 style="text-align: center; margin-bottom: 30px; color: #1d3b57; padding-left: 20px; padding-right: 20px;">Nuevo reporte de seguridad de la atención</h3>
        <hr style="border-color: #1d3b57;">
        <div style="padding-left: 20px; padding-right: 20px; margin-top: 10px;">
	        <table border="0" cellspacing="0" style="width: 100%; margin: auto;">
	        	<tbody>
	        		<tr>
	        			<td style="width: 49%; color: #1d3b57; text-transform: uppercase; font-weight: bold; padding: 5px; text-align: left;"><strong>Nombre</strong></td>
	        			<td style="width: 50%; padding: 5px; text-align: left;">{!! $nombre !!}</td>
	        		</tr>
	        		<tr>
	        			<td style="width: 49%; color: #1d3b57; text-transform: uppercase; font-weight: bold; padding: 5px; text-align: left;"><strong>Sede que reporta</strong></td>
	        			<td style="width: 50%; padding: 5px; text-align: left;">{!! $sede !!}</td>
	        		</tr>
	        		<tr>
	        			<td style="width: 49%; color: #1d3b57; text-transform: uppercase; font-weight: bold; padding: 5px; text-align: left;"><strong>Fecha de ocurrencia</strong></td>
	        			<td style="width: 50%; padding: 5px; text-align: left;">{!! $fecha !!}</td>
	        		</tr>
	        		<tr>
	        			<td style="width: 49%; color: #1d3b57; text-transform: uppercase; font-weight: bold; vertical-align: top; padding: 5px; text-align: left;"><strong>Descripción</strong></td>
	        			<td style="width: 50%; padding: 5px; text-align: left;">{!! $descripcion !!}</td>
	        		</tr>
	        		<tr>
	        			<td style="width: 49%; color: #1d3b57; text-transform: uppercase; font-weight: bold; vertical-align: top; padding: 5px; text-align: left;"><strong>Falla detectada</strong></td>
	        			<td style="width: 50%; padding: 5px; text-align: left;">{!! $falla !!}</td>
	        		</tr>
	        	</tbody>
	        </table>
        </div>
      </div>
  </body>
</html>