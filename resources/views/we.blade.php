@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
	<div class="main-container">
		<div class="row row-we">
			<div class="col-md-6">
				<img src="{{ asset('img/mision.jpg') }}" class="w-100">
			</div>
			<div class="col-md-6">
				<div class="title-we">
					Misión
					<img src="{{ asset('img/Logo_curvas.png') }}">
				</div>
				<div class="text-we">
					Brindar servicios de salud oportunos de alta calidad, mediante la atención especializada de los accidentes de tránsito, trabajo y escolares, además de la prevención y el tratamiento integral de las enfermedades profesionales.
				</div>
			</div>
		</div>
		<div class="row row-we">
			<div class="col-md-6">
				<div class="title-we title-vis">
					<img src="{{ asset('img/Logo_curvas.png') }}">
					Visión
				</div>
				<div class="text-we">
					Llegar al 2021 como una clínica reconocida en el sector de la salud por brindar la mejor atención en accidentes laborales, escolares y enfermedades profesionales
				</div>
			</div>
			<div class="col-md-6">
				<img src="{{ asset('img/vision.jpg') }}" class="w-100">
			</div>
		</div>
		<hr>
		<div class="row row-we">
			<div class="col-md-12">
				<div class="title-we">
					<img src="{{ asset('img/Logo_curvas.png') }}">
					Política Institucional de Seguridad del Paciente
				</div>
				<div class="text-we">
					En la clínica del trabajador nos comprometemos a prestar servicios de salud seguros, a través de la prevención y gestión del riesgo en la seguridad del paciente, mediante acciones asistenciales, administrativas y financieras eficientes, con talento humano competente, comprometido y humanizado; utilizando recursos tecnológicos, infraestructura y equipamiento adecuado para controlar infecciones, optimizar el uso de antibióticos. A partir del aprendizaje continuo fomentar una cultura justa, no punitiva que genere confianza en el paciente y la familia.<br><br>
					Dentro de la política de seguridad del paciente se establecen las siguientes excepciones para el acto punitivo:<br>
					Faltas graves contra los usuarios que vulneren cualquiera de sus derechos. 
					La ejecución de actos inseguros y repetitivos.<br>
					Incumplimiento de las acciones de mejoramiento planteadas desde el comité de seguridad del paciente.
				</div>
			</div>
		</div>
	</div>
@endsection