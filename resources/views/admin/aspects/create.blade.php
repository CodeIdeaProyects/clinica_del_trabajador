@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'aspects' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Crear valor</div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if($errors->count() > 0)
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['route' => 'aspects.store', 'method' => 'post']) !!}
                    @csrf
                    <div class="form-group row">
                        {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('nombre', old('nombre'), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del valor.']) !!}
                        </div>
                    </div>
                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('aspects.index') }}" class="btn btn-danger">Volver</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
