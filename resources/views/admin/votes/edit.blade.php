@extends('layouts.app')

@section('votes')
    <div class="d-none">
        {!! $site = 'votes' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Edición de votaciones <strong>{{ $vote->name }}</strong></div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if($errors->count() > 0)
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['route' => ['votes.update', $vote->id], 'method' => 'put']) !!}
                    @csrf
                    <div class="form-group row">
                        {!! Form::label('nombre_concurso', 'Nombre del concurso', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('nombre_concurso', old('nombre_concurso', $vote->name), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del concurso/campaña/votación']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('fecha_inicio', 'Fecha de inicio', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::date('fecha_inicio', old('fecha_inicio', $vote->date_start), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('fecha_finalizacion', 'Fecha de finalizacion', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::date('fecha_finalizacion', old('fecha_finalizacion', $vote->date_end), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('votacion_maxima', 'Votación máxima', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::number('votacion_maxima', old('votacion_maxima', $vote->maximum), ['class' => 'form-control', 'placeholder' => 'Número máximo de votos por usuario']) !!}
                        </div>
                    </div>
                    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('votes.index') }}" class="btn btn-danger">Volver</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
