@extends('layouts.app')

@section('site')
<div class="d-none">
    {!! $site = 'votes' !!}
</div>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Visualización votaciones</div>
        <div class="card-body">
            <a href="{{ route('votes.create') }}" class="btn btn-success"><i class="fas fa-plus-circle"></i> Crear votaciones</a>
            {!! Form::open(['route' => 'votes.index', 'method' => 'GET', 'class' => 'form-inline form-buscar']) !!}
                <div class="input-group mb-3">
                    {!! Form::text('name', $search, ['class' => 'form-control', 'placeholder' => 'Buscar votaciones...', 'aria-describedby' => 'search']) !!}
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                        @if(!is_null($search))
                            <a href="{{ route('votes.index') }}" class="btn btn-danger">
                                Limpiar <i class="fas fa-eraser"></i>
                            </a>
                        @endif
                    </div>
                </div>
            {!! Form::close() !!}
            <hr>
            @include('flash::message')
            @if($votes->count() > 0)
                <table class="table">
                    <thead>
                        <th scope="col" style="width: 30%;">Nombre</th>
                        <th scope="col" style="width: 15%;">Fecha de inicio</th>
                        <th scope="col" style="width: 15%;">Fecha fin</th>
                        <th scope="col" style="width: 10%;">Calificaciones</th>                 
                        <th scope="col" style="width: 30%;">Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($votes as $vote)
                        <tr>
                            <td>{{ $vote->name }}</td>
                            <td>{{ $vote->date_start }}</td>
                            <td>{{ $vote->date_end }}</td>
                            <td>
                                @if($vote->qualifications->count() < 2)
                                    <span class="badge badge-danger">{{ $vote->qualifications->count() }}</span>
                                @elseif($vote->qualifications->count() >= 2 && $vote->qualifications->count() < 4)
                                    <span class="badge badge-primary">{{ $vote->qualifications->count() }}</span>
                                @else
                                    <span class="badge badge-success">{{ $vote->qualifications->count() }}</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('votes.edit', $vote->id) }}" class="btn btn-info" title="Editar"><i class="fas fa-edit"></i></a>
                                <a href="{{ route('qualifications.showForm', $vote->id) }}" class="btn btn-dark" title="Ir" target="_blank"><i class="fas fa-external-link-alt"></i></a>
                                @if($vote->qualifications->count() > 0)
                                    <a href="{{ route('votes.destroy', $vote->id) }}" class="btn btn-danger disabled" title="Calificaciones ya creadas"><i class="far fa-trash-alt"></i></a>
                                    <a href="{{ route('qualifications.indexId', $vote->id) }}" class="btn btn-warning" title="Ir" target="_blank"><i class="far fa-eye"></i></a>
                                    <a href="{{ route('votes.reports', $vote->id) }}" class="btn btn-success" title="Informes"><i class="fas fa-file-excel"></i></a>
                                @else
                                    <a href="{{ route('votes.destroy', $vote->id) }}" class="btn btn-danger" title="Eliminar"><i class="far fa-trash-alt"></i></a>
                                @endif
                           </td>
                       </tr>
                       @endforeach
                   </tbody>
               </table>
            {{ $votes->setPath('')->appends(Request::except('page'))->render() }}
            @else
                <div class="alert alert-warning" role="alert">
                    No existen registros en base de datos. Para iniciar a crearlos, haz clic en <a href="{{ route('votes.create') }}" class="alert-link">Crear votaciones</a>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection