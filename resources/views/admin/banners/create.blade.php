@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'banners' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Crear banner</div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if($errors->count() > 0)
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['route' => 'banners.store', 'method' => 'post', 'files' => true]) !!}
                    @csrf
                    <div class="form-group row">
                        {!! Form::label('texto', 'Texto', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('texto', old('texto'), ['class' => 'form-control', 'placeholder' => 'Digite el texto que acompañará el banner.']) !!}
                            <small id="textHelp" class="form-text text-muted">
                                En caso de que dejes este campo vacío, no aparecerá texto en pantalla.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('link', 'Link', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('link', old('link'), ['class' => 'form-control', 'placeholder' => 'Digite el link al que redireccionará el banner.']) !!}
                            <small id="linkHelp" class="form-text text-muted">
                                En caso de que dejes este campo vacío, el banner no redireccionará.
                            </small>
                        </div>
                    </div>
                    <div class="form-group row" id="imagen_banner">
                        {!! Form::label('imagen', 'Imagen', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            <label class="label-file" for="imagen"><i class="fas fa-cloud-upload-alt"></i> Subir imagen</label>
                            <input type="file" id="imagen" name="imagen" class="d-none">
                        </div>
                    </div>
                    <div class="form-group row d-none" id="prev-image-banner">
                        {!! Form::label('previsualizacion', 'Previsualizacion', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            <img id="prev-image" class="img-preview-banner" src="">
                        </div>
                    </div>
                    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('banners.index') }}" class="btn btn-danger">Volver</a>
               {!! Form::close() !!}
           </div>
       </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $(function() {
                $('#imagen').change(function(event) {
                    addImage(event);
                    $('#prev-image-banner').removeClass('d-none');
                });
            });

            function addImage(event){
                var file = event.target.files[0],
                imageType = /image.*/;

                if (!file.type.match(imageType)){
                    return;
                }

                var reader = new FileReader();
                reader.onload = fileOnload;
                reader.readAsDataURL(file);
            }

            function fileOnload(event) {
                var result = event.target.result;
                $('#prev-image').attr("src",result);
            }
        });
    </script>
@endsection