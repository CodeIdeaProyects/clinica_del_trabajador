@extends('layouts.app')

@section('site')
  <div class="d-none">
      {!! $site = 'beginings' !!}
  </div>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Principios</div>
        <div class="card-body">
            <a href="{{ route('beginings.create') }}" class="btn btn-success"><i class="fas fa-plus-circle"></i> Crear principios</a>
            {!! Form::open(['route' => 'beginings.index', 'method' => 'GET', 'class' => 'form-inline form-buscar']) !!}
              <div class="input-group mb-3">
                  {!! Form::text('name', $search, ['class' => 'form-control', 'placeholder' => 'Buscar principios...', 'aria-describedby' => 'search']) !!}
                  <div class="input-group-append">
                      <button class="btn btn-success" type="submit">
                          <i class="fas fa-search"></i>
                      </button>
                      @if(!is_null($search))
                          <a href="{{ route('beginings.index') }}" class="btn btn-danger">
                              Limpiar <i class="fas fa-eraser"></i>
                          </a>
                      @endif
                  </div>
              </div>
            {!! Form::close() !!}
            <hr>
            @include('flash::message')
            @if($beginings->count() > 0)
              <table class="table">
                  <thead>
                      <th scope="col" style="width: 75%;">Nombre</th>
                      <th scope="col" style="width: 25%;">Acciones</th>
                  </thead>
                  <tbody>
                      @foreach($beginings as $begining)
                        <tr>
                            <td>{{ $begining->name }}</td>
                            <td>
                                <a href="{{ route('beginings.edit', $begining->id) }}" class="btn btn-info" title="Editar"><i class="fas fa-edit"></i></a>
                                @if($begining->qualifications->count() > 0)
                                  <a href="{{ route('beginings.destroy', $begining->id) }}" class="btn btn-danger disabled" title="Calificaciones asociadas"><i class="far fa-trash-alt"></i></a>
                                @else
                                  <a href="{{ route('beginings.destroy', $begining->id) }}" class="btn btn-danger" title="Eliminar"><i class="far fa-trash-alt"></i></a>
                                @endif
                            </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              {{ $beginings->setPath('')->appends(Request::except('page'))->render() }}
            @else
              <div class="alert alert-warning" role="alert">
                No existen registros en base de datos. Para iniciar a crearlos, haz clic en <a href="{{ route('beginings.create') }}" class="alert-link">Crear principios</a>
              </div>
            @endif
        </div>
    </div>
</div>
@endsection