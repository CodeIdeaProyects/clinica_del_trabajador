@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
    <div id="login-container">
        <div class="main-container-auth">
            <div class="card-container">
                <div class="card" id="card-login">
                    <div class="card-header">Cambio de contraseña</div>
                    <div class="card-body">
                        @if($errors->count() > 0)
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @include('flash::message')
                        {!! Form::open(['route' => 'users.password', 'method' => 'POST']) !!}
                            @csrf
                            <div class="form-group row-login">
                                {!! Form::label('contrasena_actual', 'Contraseña actual') !!}
                                {!! Form::password('contrasena_actual', ['class' => 'form-control', 'placeholder' => 'Digite la contraseña actual']) !!}
                            </div>
                            <div class="form-group row-login">
                                {!! Form::label('contrasena', 'Contraseña') !!}
                                {!! Form::password('contrasena', ['class' => 'form-control', 'placeholder' => 'Digite la nueva contraseña']) !!}
                            </div>
                            <div class="form-group row-login">
                                {!! Form::label('contrasena_confirmation', 'Confirme la contraseña') !!}
                                {!! Form::password('contrasena_confirmation', ['class' => 'form-control', 'placeholder' => 'Digite nuevamente la contraseña del usuario.']) !!}
                            </div>
                            {!! Form::submit('Guardar', ['class' => 'btn btn-clínica_del_trabajador']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
