@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'users' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Usuarios</div>
            <div class="card-body">
                <a href="{{ route('users.create') }}" class="btn btn-success"><i class="fas fa-plus-circle"></i> Crear usuarios</a>
                {!! Form::open(['route' => 'users.index', 'method' => 'GET', 'class' => 'form-inline form-buscar']) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('name', $search, ['class' => 'form-control', 'placeholder' => 'Buscar usuario...', 'aria-describedby' => 'search']) !!}
                        <div class="input-group-append">
                            <button class="btn btn-success" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                            @if(!is_null($search))
                                <a href="{{ route('users.index') }}" class="btn btn-danger">
                                    Limpiar <i class="fas fa-eraser"></i>
                                </a>
                            @endif
                        </div>
                    </div>
                {!! Form::close() !!}

                <hr>
                @include('flash::message')
                @if($users->count() > 0)
                    <table class="table">
                        <thead>
                            <th scope="col" style="width: 20%;">Nombre</th>
                            <th scope="col" style="width: 25%;">Email</th>
                            <th scope="col" style="width: 20%;">Identificación</th>
                            <th scope="col" style="width: 10%;">Rol</th>
                            <th scope="col" style="width: 10%;">Estado</th>
                            <th scope="col" style="width: 15%;">Acciones</th>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                @if($user->state === 'Inactive')
                                    <tr class="table-active">
                                @else
                                    <tr>
                                @endif
                                    <td>{{ $user->name }} {{ $user->lastname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->identification_type }} - {{ $user->identification_number }}</td>
                                    <td>
                                        @if($user->user_type === 'root')
                                            <span class="badge badge-success">Super admin</span>
                                        @elseif($user->user_type === 'admin')
                                            <span class="badge badge-primary">Administrador</span>
                                        @else
                                            <span class="badge badge-info">Usuario</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($user->state === 'Active')
                                            <span class="badge badge-success">Activo</span>
                                        @else
                                            <span class="badge badge-danger">Inactivo</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info" title="Editar"><i class="fas fa-edit"></i></a>
                                        @if($user->state === 'Inactive')
                                            <a href="{{ route('users.destroy', $user->id) }}" class="btn btn-success" title="Activar"><i class="fas fa-sync-alt"></i></a>
                                        @else
                                            <a href="{{ route('users.destroy', $user->id) }}" class="btn btn-danger" title="Inactivar"><i class="fas fa-sync-alt"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $users->setPath('')->appends(Request::except('page'))->render() }}
                @else
                    <div class="alert alert-warning" role="alert">
                        No existen registros en base de datos. Para iniciar a crearlos, haz clic en <a href="{{ route('users.create') }}" class="alert-link">Crear usuarios</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
