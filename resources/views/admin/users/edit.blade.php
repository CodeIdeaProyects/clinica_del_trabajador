@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'users' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Editar usuario: <strong>{{ $user->name }}</strong></div>
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if($errors->count() > 0)
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['route' => ['users.update', $user->id], 'method' => 'put']) !!}
                    @csrf
                    <div class="form-group row">
                        {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('nombre', old('nombre', $user->name), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del usuario.']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('apellido', 'Apellido', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('apellido', old('apellido', $user->lastname), ['class' => 'form-control', 'placeholder' => 'Digite el nombre del usuario.']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('email', 'Email', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::email('email', old('email', $user->email), ['class' => 'form-control', 'placeholder' => 'Digite el correo electrónico del usuario.', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('tipo_identificacion', 'Tipo de identificación', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('tipo_identificacion', ['CC' => 'Cédula de ciudadanía', 'CE' => 'Cédula de extranjería', 'TI' => 'Tarjeta de identidad', 'PA' => 'Pasaporte'], old('tipo_identificacion', $user->identification_type), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('funciones', 'Funciones del usuario', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('funciones', ['Administrativas' => 'Administrativas', 'Asistenciales' => 'Asistenciales', 'Servicios de apoyo' => 'Servicios de apoyo'], old('funciones',$user->function), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('numero_identificacion', 'Numero de identificación', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('numero_identificacion', old('numero_identificacion', $user->identification_number), ['class' => 'form-control', 'placeholder' => 'Digite el numero de identificación del usuario.']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('genero', 'Genero', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="radio col-sm-10">
                            <input type="radio" name="genero" value="M" @if((old('genero') === 'M') || ($user->gender === 'M')) checked @endif class="form-control" id="genero_male">
                            {!! Form::label('genero_male', 'Masculino') !!}
                            <input type="radio" name="genero" value="F" @if((old('genero') === 'F') || ($user->gender === 'F')) checked @endif class="form-control" id="genero_female">
                            {!! Form::label('genero_female', 'Femenino') !!}
                            <input type="radio" name="genero" value="O" @if((old('genero') === 'O')  || ($user->gender === 'O')) checked @endif class="form-control" id="genero_otro">
                            {!! Form::label('genero_otro', 'Otro') !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('rol', 'Rol', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('rol', ['root' => 'Superadmin', 'admin' => 'Admin', 'user' => 'Usuario'], old('rol', $user->user_type), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento', ['class' => 'col-sm-2 col-form-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::date('fecha_nacimiento', old('fecha_nacimiento', $user->birthday), ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                        </div>
                    </div>
                    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('users.index') }}" class="btn btn-danger">Volver</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
