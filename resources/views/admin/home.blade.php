@extends('layouts.app')

@section('site')
    <div class="d-none">
        {!! $site = 'home' !!}
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @if(Auth::user()->isRoot())
                        <div class="col-md-3">
                            <a href="{{ route('users.index') }}" class="link-card-home">
                                <div class="card-home">
                                    <i class="fas fa-users"></i><br>
                                    Usuarios
                                </div>
                            </a>
                        </div>
                    @else
                        <div class="col-md-3">
                            <a href="#" class="link-card-home disabled">
                                <div class="card-home">
                                    <i class="fas fa-users"></i><br>
                                    Usuarios
                                </div>
                            </a>
                        </div>
                    @endif
                    <div class="col-md-3">
                        <a href="{{ route('categories.index') }}" class="link-card-home">
                            <div class="card-home">
                                <i class="fas fa-tags"></i><br>
                                Procesos
                            </div>
                        </a>    
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('types.index') }}" class="link-card-home">
                            <div class="card-home">
                                <i class="fas fa-list"></i><br>
                                Tipos
                            </div>
                        </a>    
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('forms.index') }}" class="link-card-home">
                            <div class="card-home">
                                <i class="fas fa-clipboard-list"></i><br>
                                Documentos
                            </div>
                        </a>    
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ route('aspects.index') }}" class="link-card-home">
                            <div class="card-home">
                                <i class="far fa-list-alt"></i><br>
                                Valores
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('beginings.index') }}" class="link-card-home">
                            <div class="card-home">
                                <i class="far fa-list-alt"></i><br>
                                Principios
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('votes.index') }}" class="link-card-home">
                            <div class="card-home">
                                <i class="fas fa-person-booth"></i><br>
                                C. excelencia
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('banners.index') }}" class="link-card-home">
                            <div class="card-home">
                                <i class="fas fa-images"></i><br>
                                Banners
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ route('polls.index') }}" class="link-card-home">
                            <div class="card-home">
                                <i class="fas fa-poll"></i><br>
                                Encuestas
                            </div>
                        </a>
                    </div>
                    @if(Auth::user()->isRoot())
                        <div class="col-md-3">
                            <a href="{{ route('main.backup') }}" class="link-card-home">
                                <div class="card-home">
                                    <i class="fas fa-database"></i><br>
                                    Backup
                                </div>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection