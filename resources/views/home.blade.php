@extends('layouts.main')

@section('menu')
    <div class="d-none">
        {!! $site = 'ext' !!}
    </div>
@endsection

@section('content')
    <div class="main-container">
        @if(Auth::user()->isRoot() || Auth::user()->isAdmin())
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="wellcome-text">
                        Bienvenido a la intranet de la Clínica del trabajador.<br>
                        Selecciona uno de los macroprocesos para ver la información.<br>
                        Si deseas acceder al panel administrativo, haz clic en el siguiente botón<br>
                        <a href="{{ route('admin') }}" class="btn btn-clínica_del_trabajador">Panel administrativo</a>
                    </div>
                </div>
            </div>
        @else
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="wellcome-text">
                        Bienvenido a la intranet de la Clínica del trabajador.<br>
                        Selecciona uno de los macroprocesos para ver la información.
                    </div>
                </div>
            </div>
        @endif
        <hr>
        <div id="process-image">
            <img src="{{ asset('img/mapa_procesos.jpg') }}" class="w-100">
            <a href="{{ url('/') }}/user/macroprocesos/humanizacion" class="process process1"></a>
            <a href="{{ url('/') }}/user/macroprocesos/seguridad-de-la-atencion" class="process process2"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-del-riesgo" class="process process3"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-de-la-informacion-y-la-tecnologia" class="process process4"></a>
            <a href="{{ url('/') }}/user/macroprocesos/gestion-financiera" class="process process5"></a>
            <a href="{{ url('/') }}/user/macroprocesos/ambiente-fisico" class="process process6"></a>
            <a href="{{ url('/') }}/user/macroprocesos/direccionamiento-estrategico" class="process process7"></a>
            <a href="{{ url('/') }}/user/macroprocesos/desarrollo-del-talento-humano" class="process process8"></a>
            <a href="{{ url('/') }}/user/macroprocesos/quirurgicos" class="process process9"></a>
            <a href="{{ url('/') }}/user/macroprocesos/consulta-externa" class="process process10"></a>
            <a href="{{ url('/') }}/user/macroprocesos/apoyo-diagnostico" class="process process11"></a>
            <a href="{{ url('/') }}/user/macroprocesos/urgencias" class="process process12"></a>
            <a href="{{ url('/') }}/user/macroprocesos/internacion" class="process process13"></a>
            <a href="{{ url('/') }}/user/macroprocesos/orientacion-al-usuario" class="process process14"></a>
        </div>
    </div>
@endsection
