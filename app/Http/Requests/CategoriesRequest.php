<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':{
                return [
                    'nombre'                    =>  'required|min:3|max:120|string',
                    'tipo_proceso'              =>  'required',
                    'macroproceso'              =>  'nullable|required_if:tipo_proceso,Hija',
                    'codificacion'              =>  'nullable|required_if:tipo_proceso,Hija',
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'nombre'                    =>  'required|min:3|max:120|string',
                    'tipo_proceso'              =>  'required',
                    'macroproceso'              =>  'nullable|required_if:tipo_proceso,Hija',
                    'codificacion'              =>  'nullable|required_if:tipo_proceso,Hija',
                ];    
            }
        }
    }
}
