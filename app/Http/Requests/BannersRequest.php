<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':{
                return [
                    'imagen'    =>  'required|image',
                    'texto'     =>  'nullable|min:3|max:120',
                    'link'      =>  'nullable|url',
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'imagen'    =>  'nullable|image',
                    'texto'     =>  'nullable|min:3|max:120',
                    'link'      =>  'nullable|url',
                ];    
            }
        }
    }
}
