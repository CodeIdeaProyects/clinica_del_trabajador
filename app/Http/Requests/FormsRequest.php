<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':{
                return [
                    'nombre'                    =>  'required|min:3|max:120|string',
                    'macroproceso'              =>  'required',
                    'subproceso'                =>  'required',
                    'tipo_formulario'           =>  'required',
                    'codificacion'              =>  'required',
                    'codigo'                    =>  'nullable|required_if:codificacion,Manual',
                    'evidencia'                 =>  'required',
                ];
            }
            case 'PUT':
            case 'PATCH':{
                return [
                    'nombre'                    =>  'required|min:3|max:120|string',
                    'codigo'                    =>  'nullable|required_if:codificacion,Manual',
                    'evidencia'                 =>  'nullable',
                    'observacion'               =>  'required',
                ];    
            }
        }
    }
}
