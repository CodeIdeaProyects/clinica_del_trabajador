<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Qualification;
use App\User;
use App\Aspect;
use App\Begining;
use Excel;
use Auth;
use App\Http\Requests\QualificationsRequest;
use Illuminate\Support\Facades\DB;
use App\Vote;
use App\Support\Collection;
use Mail;

class QualificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }
    public function indexId($id)
    {
        $vote = Vote::find($id);
        $qualifications = (new Collection($vote->qualifications))->paginate(15);
        $rank= DB::select('SELECT count(user_id_qualified) as user_count,users.name,user_id_qualified,vote_id FROM qualifications JOIN users ON qualifications.user_id_qualified=users.id WHERE vote_id='.$id.' GROUP BY users.name,user_id_qualified,vote_id ORDER BY user_count DESC');
        $rankings = (new Collection($rank))->paginate(15);
        
        return view('admin.qualifications.index')->with('qualifications', $qualifications)->with('rankings',$rankings);  
  }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QualificationsRequest $request)
    {
        $vote = Vote::find($request->votacion);

        if((now()->toDateString() >= $vote->date_start) && (now()->toDateString() <= $vote->date_end)){
            $can = true;
        }else{
            $can = false;
        }

        if(is_null($vote)){
            abort(403, 'La votación a la que estás tratando de acceder no existe.');
        }elseif(!$can){
            abort(403, 'Debes acceder en las fechas indicadas.');
        }else{
            $qualification = Qualification::where('vote_id', $vote->id)->where('user_id', Auth::user()->id)->get();
            if($qualification->count() >= $vote->maximum){
                abort(403, 'Ya realizaste el proceso de votación. Disponías de un máximo de ' . $vote->maximum . ' votos.');
            }else{
                $qualification = new Qualification();
                $user = User::find($request->usuario);
                $aspect = Aspect::find($request->aspecto);
                $begining = Begining::find($request->principio);
                $qualification->user()->associate(Auth::user());
                $qualification->userQ()->associate($user);
                $qualification->aspect()->associate($aspect);
                $qualification->begining()->associate($begining); 
                $qualification->vote()->associate($vote);
                $qualification->justify = $request->justificacion;

                try{
                    $qualification->save();
                    $flag = true;
                    $asunto = "¡Felicidades! Fuiste seleccionado por un compañero";
                    $array = array(
                        'nombre' => $qualification->userQ->name,
                        'nombre2' => $qualification->user->name,
                        'valor' => $qualification->aspect->name,
                        'principio' => $qualification->begining->name,
                        'justificacion' => $qualification->justify,
                    );
                    Mail::send('mailers.votingNotification', $array, function($msj) use ($array, $asunto, $user){
                        $msj->subject((String)$asunto);
                        $msj->to((String)$user->email);
                    });
                    // $qualification->sendMail($user->email);
                }catch(Exception $e){
                    $flag = false;
                }

                return view('users.qualifications.result')->with('flag', $flag);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showForm($id)
    {
        $vote = Vote::find($id);

        if((now()->toDateString() >= $vote->date_start) && (now()->toDateString() <= $vote->date_end)){
            $can = true;
        }else{
            $can = false;
        }

        if(is_null($vote)){
            abort(403, 'La votación a la que estás tratando de acceder no existe.');
        }elseif(!$can){
            abort(403, 'Debes acceder en las fechas indicadas.');
        }else{
            $qualification = Qualification::where('vote_id', $vote->id)->where('user_id', Auth::user()->id)->get();
            if($qualification->count() >= $vote->maximum){
                abort(403, 'Ya realizaste el proceso de votación. Disponías de un máximo de ' . $vote->maximum . ' votos.');
            }else{
                $users = User::select(DB::raw("CONCAT(name,'- ',function) AS name"),'id')->where('user_type', 'user')->where('id', '<>', Auth::user()->id)->orderBy('name','ASC')->pluck('name','id');
                $aspects = Aspect::orderBy('name', 'ASC')->pluck('name', 'id');
                $beginings = Begining::orderBy('name', 'ASC')->pluck('name', 'id');
                
                return view('users.qualifications.index')->with('users', $users)->with('aspects', $aspects)->with('beginings', $beginings)->with('vote', $vote);
            }
        }
    }
}