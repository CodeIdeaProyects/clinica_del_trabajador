<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Http\Requests\TypesRequest;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $types = Type::search($request->name)->orderBy('id', 'ASC')->paginate(20);
        $search = $request->name;

        return view('admin.types.index')->with('types', $types)->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TypesRequest $request)
    {
        $type = new Type();
        $type->name = $request->nombre;
        $type->code = $request->codificacion;

        try{
            $type->save();

            flash('Tipo de formulario creado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al crear el tipo de formulario')->error()->important();
        }

        return redirect()->route('types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = Type::find($id);

        return view('admin.types.edit')->with('type', $type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TypesRequest $request, $id)
    {
        $type = Type::find($id);
        $type->name = $request->nombre;
        $type->code = $request->codificacion;

        try{
            $type->save();

            flash('Tipo de formulario actualizado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al actualizar el tipo de formulario')->error()->important();
        }

        return redirect()->route('types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = Type::find($id);

        if($type->forms->count() > 0){
            flash('Error al eliminar el tipo de formulario, tiene formularios asociados')->error()->important();
        }else{
            try{
                $type->delete();

                flash('Tipo de formulario eliminado correctamente')->success()->important();
            }catch(Exception $e){
                flash('Error al eliminar el estado del usuario')->error()->important();
            }
        }

        return redirect()->route('types.index');
    }
}
