<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Http\Requests\UsersRequest;
use App\Http\Requests\PasswordRequest;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::search($request->name)->orderBy('id', 'ASC')->paginate(20);
        $search = $request->name;
       
        return view('admin.users.index')->with('users', $users)->with('search', $search);
    }
    public function backup()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        $user = new User();
        $user->name = $request->nombre;
        $user->lastname = $request->apellido;
        $user->email = $request->email;
        $user->function = $request->funciones;
        $user->password = bcrypt($request->contrasena);
        $user->birthday = $request->fecha_nacimiento;
        $user->identification_type = $request->tipo_identificacion;
        $user->identification_number = $request->numero_identificacion;
        $user->user_type = $request->rol;
        $user->gender = $request->genero;

        try{
            $user->save();

            flash('Usuario creado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al crear el usuario')->error()->important();
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    public function getUserByFuntion(Request $request, $function){
        $users = user::where('function', $function)->get();  

        return response()->json($users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change(){
        return view('admin.users.change');
    }

    public function password(PasswordRequest $request){
        $user = Auth::user();
        if(password_verify($request->contrasena_actual, $user->password)){
            $user->password = bcrypt($request->contrasena);
            try{
                $user->save();

                flash('El cambio de contraseña exitoso!')->success()->important();
            }catch(Exception $e){
                flash('Error en el cambio de contraseña')->error()->important();
            }
        }else{
            flash('La contraseña actual no coincide')->error()->important();
        }

        return redirect()->route('users.change');
    }
    

    public function update(UsersRequest $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->nombre;
        $user->lastname = $request->apellido;
        $user->birthday = $request->fecha_nacimiento;
        $user->identification_type = $request->tipo_identificacion;
        $user->identification_number = $request->numero_identificacion;
        $user->function = $request->funciones;
        $user->user_type = $request->rol;
        $user->gender = $request->genero;

        try{
            $user->save();

            flash('Usuario actualizado exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al actualizar el usuario')->error()->important();
        }

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if($user->state === 'Active'){
            $user->state = 'Inactive';
        }else{
            $user->state = 'Active';
        }

        try{
            $user->save();

            flash('El estado del usuario ' . $user->name . ' cambió exitosamente')->success()->important();
        }catch(Exception $e){
            flash('Error al cambiar el estado del usuario')->error()->important();
        }

        return redirect()->route('users.index');
    }
}
