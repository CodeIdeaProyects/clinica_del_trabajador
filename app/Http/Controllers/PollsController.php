<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poll;
use App\Http\Requests\PollRequest;

class PollsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $polls = Poll::orderBy('id', 'DESC')->paginate(20);
        return view('admin.polls.index')->with('polls', $polls);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.polls.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PollRequest $request)
    {
        $poll = new Poll();
        $poll->name = $request->nombre;
        $poll->link = $request->enlace;

        try{
            $poll->save();

            flash('Encuesta creada exitosamente ')->success()->important();
        }catch(Exception $e){
            flash('Error al crear la encuesta')->error()->important();
        }

        return redirect()->route('polls.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $poll = Poll::find($id);

            try{
                $poll->delete();

                flash('link encuesta eliminado correctamente')->success()->important();
            }catch(Exception $e){
                flash('Error al eliminar el link')->error()->important();
            }
        

        return redirect()->route('polls.index');
    }

    public function showPolls(){
        $polls = Poll::orderBy('created_at', 'ASC')->get();

        return view('users.polls')->with('polls', $polls);
    }
}
