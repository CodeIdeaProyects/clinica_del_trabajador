<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'types';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code'
    ];

    public function forms(){
        return $this->hasMany('App\Form');
    }
    public function scopeSearch($query, $name){
        return $query->where('name', 'LIKE', "%$name%");
    }
}
