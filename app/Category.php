<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    protected $table = 'categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'name',' image', 'code'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function category(){
    	return $this->belongsTo('App\Category');
    }

    public function categories(){
    	return $this->hasMany('App\Category');
    }

    public function forms(){
        return $this->hasMany('App\Form');
    }
    public function scopeSearch($query, $name){
        return $query->where('name', 'LIKE', "%$name%");
    }
}
