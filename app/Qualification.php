<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\VotingNotification;

class Qualification extends Model
{
	protected $table = 'qualifications';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id', 'user_id_qualified', 'aspect_id', 'vote_id', 'justify'
    ];

	public function user(){
		return $this->belongsTo('App\User','user_id');
	}

	public function userQ(){
		return $this->belongsTo('App\User','user_id_qualified');
	}

	public function aspect(){
		return $this->belongsTo('App\Aspect');
	}
	
	public function vote(){
		return $this->belongsTo('App\Vote');
	}
	public function begining(){
		return $this->belongsTo('App\Begining');
	}
}
