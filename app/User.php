<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'birthday', 'identification_type', 'identification_number', 'user_type', 'gender', 'state', 'function',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function forms(){
        return $this->hasMany('App\Form');
    }
    
    public function qualification(){
        return $this->hasMany('App\Qualification','user_id');
    }

    public function qualificationQ(){
        return $this->hasMany('App\Qualification','user_id_qualified');
    }

    public function isAdmin(){
        return $this->user_type === 'admin';
    }

    public function isRoot(){
        return $this->user_type === 'root';
    }
    public function scopeSearch($query, $name){
        return $query->where('name', 'LIKE', "%$name%");
    }
}
