<?php

namespace App\Exports;

use App\Form;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;


class FormsExport implements FromCollection,WithHeadings
{
	use Exportable;
    protected $forms;
    public function __construct($forms = null)
    {
        $this->forms = $forms;
    }
public function headings(): array
        {
            return [
                'Nombre',
                'Subproceso',
                'Proceso',
                'Usuario',
                'Tipo',
                'Version',
                'Codigo',
                'Observacion',
                'fecha'
            ];
        }


    public function collection()
    {
        return $this->forms;
    }
}
