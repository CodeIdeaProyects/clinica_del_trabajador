<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Begining extends Model
{
    protected $table = 'beginings';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function qualifications(){
        return $this->hasMany('App\Qualification');
	}
    public function scopeSearch($query, $name){
        return $query->where('name', 'LIKE', "%$name%");
    }
}
