<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
	protected $table = 'votes';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'name', 'date_start', 'date_end', 'maximum'
	];

     public function qualifications(){
        return $this->hasMany('App\Qualification');
    }
     public function scopeSearch($query, $name){
        return $query->where('name', 'LIKE', "%$name%");
    }
}
