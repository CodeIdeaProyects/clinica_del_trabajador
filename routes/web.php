<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', [
		'uses' => 'MainController@index',
		'as' => 'index']);

Route::get('/nosotros', [
		'uses' => 'MainController@we',
		'as' => 'we']);
Route::post('contacto', [
		'uses' => 'MainController@mail',
		'as' => 'contacto.mail']);

Route::get('/novedades/{site}', [
		'uses' => 'MainController@news',
		'as' => 'news']);

Route::group(['prefix' => 'admin', 'middleware' => ['auth','admin']], function(){
	Route::get('/home', 'HomeController@admin')->name('admin');
	
	// Categorías
	Route::resource('categories','CategoriesController');
	Route::get('categories/{id}/destroy', [
		'uses' => 'CategoriesController@destroy',
		'as' => 'categories.destroy']);
	Route::get('/getProcess/{id}', "CategoriesController@getProcessById")->name('getProcess');
	// Fin Categorías
	// Tipos
	Route::resource('types','TypesController');
	Route::get('types/{id}/destroy', [
		'uses' => 'TypesController@destroy',
		'as' => 'types.destroy']);
	// Fin Tipos
	// Banners
	Route::resource('banners','BannersController');
	Route::get('banners/{id}/destroy', [
		'uses' => 'BannersController@destroy',
		'as' => 'banners.destroy']);
	// Fin Banners
	// Aspects
	Route::resource('aspects','AspectsController');
	Route::get('aspects/{id}/destroy', [
		'uses' => 'AspectsController@destroy',
		'as' => 'aspects.destroy']);
	// Fin Banners
	// Forms
	Route::resource('forms','FormsController');
	Route::get('forms/{id}/destroy', [
		'uses' => 'FormsController@destroy',
		'as' => 'forms.destroy']);
	Route::get('forms/{id}/history', [
		'uses' => 'FormsController@history',
		'as' => 'forms.history']);
	Route::get('forms/{id}/reports', [
		'uses' => 'FormsController@reports',
		'as' => 'forms.reports']);
	Route::get('reportsDocuments', [
		'uses' => 'FormsController@reportsDocuments',
		'as' => 'forms.reportsDocuments']);
	// Fin Forms
	// Calificaciones
	Route::resource('qualifications','QualificationsController');
	Route::get('qualifications/{id}/destroy', [
		'uses' => 'QualificationsController@destroy',
		'as' => 'qualifications.destroy']);
	Route::get('qualifications/{id}/indexId', [
		'uses' => 'QualificationsController@indexId',
		'as' => 'qualifications.indexId']);

	// Fin Calificaciones
	// Votos
	Route::resource('votes','VotesController');
	Route::get('votes/{id}/destroy', [
		'uses' => 'VotesController@destroy',
		'as' => 'votes.destroy']);
	Route::get('votes/{id}/reports', [
		'uses' => 'VotesController@reports',
		'as' => 'votes.reports']);
	// Fin Votos
	//export calificacion
	
    //Principios
    Route::resource('beginings','BeginingsController');
	Route::get('beginings/{id}/destroy', [
		'uses' => 'BeginingsController@destroy',
		'as' => 'beginings.destroy']);
	// Fin Principios
	// Encuestas
	Route::resource('polls','PollsController');
	Route::get('polls/{id}/destroy', [
		'uses' => 'PollsController@destroy',
		'as' => 'polls.destroy']);	
	// Fin encuestas
});

Route::group(['prefix' => 'root', 'middleware' => ['auth','root']], function(){
// Usuarios
	Route::resource('users','UsersController');
	Route::get('users/{id}/destroy', [
		'uses' => 'UsersController@destroy',
		'as' => 'users.destroy']);
	Route::get('change', [
		'uses' => 'UsersController@change',
		'as' => 'users.change']);
	Route::post('password', [
		'uses' => 'UsersController@password',
		'as' => 'users.password']);
	// Fin Usuarios
	Route::get('backup', [
		'uses' => 'MainController@backup',
		'as' => 'main.backup']);
});


Route::group(['prefix' => 'user', 'middleware' => ['auth']], function(){
	// Calificaciones
	Route::get('qualifications/{id}/form', [
		'uses' => 'QualificationsController@showForm',
		'as' => 'qualifications.showForm']);
	Route::post('qualifications/store', 'QualificationsController@store')->name('qualifications.store');
	// Fin Calificaciones
	// Categorías
	Route::get('macroprocesos/{slug}', [
		'uses' => 'CategoriesController@show',
		'as' => 'categories.show']);
	// Fin Categorías
	// Subprocesos
	Route::get('macroprocesos/{macro}/{sub}', [
		'uses' => 'CategoriesController@showSubprocess',
		'as' => 'categories.show.subprocess']);
	// Fin Subprocesos
	// PDF
	Route::get('pdfview/{id}', [
		'uses' => 'CategoriesController@showPDF',
		'as' => 'categories.show.pdf']);
	// Fin PDF
	//Scope formulario
	Route::get('scopeF', [
		'uses' => 'FormsController@scopeF',
		'as' => 'forms.scopeF']);
    
    //cambio contraseña
	Route::get('change', [
		'uses' => 'UsersController@change',
		'as' => 'users.change']);
	Route::post('password', [
		'uses' => 'UsersController@password',
		'as' => 'users.password']);

	Route::get('/getUsers/{id}', "UsersController@getUserByFuntion")->name('getUsers');

	// Encuestas
	Route::get('encuestas', [
		'uses' => 'PollsController@showPolls',
		'as' => 'polls.show.user']);
});