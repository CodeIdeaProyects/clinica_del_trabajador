<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         User::create(['name' => 'Cesar Armando','lastname' => 'Rodiguez', 'email' => 'siscesaroal@gmail.com', 'password' => bcrypt('1052406171'), 'birthday' => '1996-01-07', 'identification_number' => '1052406171', 'identification_type' => 'CC', 'gender' => 'M', 'user_type' => 'root','function'=>'Administrativas']);
         User::create(['name' => 'Luis Diego','lastname' => 'Ojeda', 'email' => 'luisdiego_ojeda@hotmail.com', 'password' => bcrypt('1049644489'), 'birthday' => '1996-01-07', 'identification_number' => '1049644489', 'identification_type' => 'CC', 'gender' => 'M', 'user_type' => 'user','function'=>'Administrativas']);
    }
}
