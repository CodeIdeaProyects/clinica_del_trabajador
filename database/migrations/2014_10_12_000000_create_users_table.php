<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',120);
            $table->string('lastname',120);
            $table->string('email',120)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password',132);
            $table->enum('function',['Administrativas','Asistenciales','Servicios de apoyo']);
            $table->date('birthday');
            $table->enum('identification_type',['CC','CE','TI','PA']);
            $table->string('identification_number',15);
            $table->enum('user_type',['root','admin','user']);
            $table->enum('gender',['M','F','O']);
            $table->enum('state',['Active','Inactive'])->default('Active');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
